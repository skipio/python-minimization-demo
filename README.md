## Optimization

This project demonstrates approaches to find the minimum cost of a given selection of weighted elements given a selection of constraints.

For instance given items **_[A, B, C, D, E]_** of different costs and with different qualities, we can find the ratio of these that will meet all the quality constraints **_[W, X, Y, Z]_** at minimum cost.

|      Element      |    Cost |       W |       X |       Y |      Z | Final Fraction |
| :---------------: | ------: | ------: | ------: | ------: | -----: | -------------: |
|         A         |      40 |      15 |      15 |       5 |     10 |         62.71% |
|         B         |      30 |      20 |       3 |       0 |      2 |          1.69% |
|         C         |      70 |      15 |       5 |      20 |      5 |         33.90% |
|         D         |      20 |      10 |       2 |       5 |      0 |          1.69% |
|         E         |      80 |       0 |      10 |       5 |     30 |          0.00% |
|    **Target**     | **min** |  **15** |  **11** |  **10** |  **8** |                |
| **_Final value_** | _49.66_ | _15.00_ | _11.19_ | _10.00_ | _8.00_ |      _100.00%_ |

Output is a list of fractions of all elements **_A_** thru **_E_**
