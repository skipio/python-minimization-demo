from functools import reduce
from random import random
from scipy.optimize import LinearConstraint, minimize


class Element:
    def __init__(self, cost, w, x, y, z):
        self.cost = cost
        self.w = w
        self.x = x
        self.y = y
        self.z = z


a = Element(40, 15, 15, 5, 10)
b = Element(30, 20, 3, 0, 2)
c = Element(70, 15, 5, 20, 5)
d = Element(20, 10, 2, 5, 0)
e = Element(80, 0, 10, 5, 30)

elements = [a, b, c, d, e]
quantities = [0, 0, 0, 0, 0]

target = {
    "w": 15,
    "x": 11,
    "y": 10,
    "z": 8,
}


def randomize_quantities():
    # randomize the quantities of each of the 5 elements
    quantities[0] = round(random() * 100, 2)
    total = 100 - quantities[0]
    for i in range(1, len(elements) - 1):
        quantities[i] = round(random() * total, 2)
        total -= quantities[i]
    quantities[-1] = 100 - sum(quantities[:-1])


# array of names of each quantity to be optimized
vals = [j for j in dir(elements[0]) if not j.startswith("_")]


def calc(quant, val):
    return reduce(
        lambda total, l: total + getattr(l[0], val) * l[1] / 100,
        zip(elements, quant),
        0,
    )


randomize_quantities()

"""
Bounds:
1. A - E: q must be 0 >= q >= 100
2. Sum (A - E: q) must equal 100
3. Target values w - z are met
Others: TBD
eg. mmust have 10% D
"""
# bound each quantity between 0 and 100
bounds = [(0, 100) for _ in range(len(elements))]

# constrain the sum of quantities to 100
constraints = [{"type": "eq", "fun": lambda x: sum(x) - 100.0}]

# add target constraints highlighted above
for t in target:
    constraints.append(
        {"type": "ineq", "fun": lambda x, t: calc(x, t) - target[t], "args": t}
    )

# Apply minimizing function using set cost function, bounds and constraints
result = minimize(
    lambda x: calc(x, "cost"),
    quantities,
    bounds=bounds,
    constraints=constraints,
)

# Output values in readable format
print(result)
print()

for v in vals:
    c = round(calc(result.x, v), 2)
    if v == "cost":
        print("Cost: {}".format(c))
    else:
        print("Value {} with target {} is {}".format(v, target[v], c))

print()

for (x, k) in zip(result.x, ["a", "b", "c", "d", "e"]):
    print("Element {}: {}%".format(k, round(x, 2)))
